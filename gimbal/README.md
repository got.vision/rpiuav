# Camera mount / Gimbal
Consisting of simple pan/tilt set from Adafruit (can be modelled to own version), base holder, and camera holder

## Pan/tilt set
[Pan/Tilt set from Adafruit](https://www.adafruit.com/product/1967) used for now. Should be possible to simplify.

## Base holder
TODO

## Camera holder for RPi v2 camera
Using model from [Thingiverse](https://www.thingiverse.com/thing:3004766/files) and user [_Oo](https://www.thingiverse.com/_0o/designs)

# Future improvements
## Simple self built model
This camera setup can be simplified by building own stand based on standard RC servo(es), or mini RC servo(es). Can also be further simplified by only having a tilt setup, and let the UAV do the paning.

## Stabilized Gimbal
Vibrations from the UAV can probably be ignored. But if turn out to be problematic, physical dampnening can be considerd. Or a digital stabilizer can be introduzed. Another problem with stabilization ir the camera movement when the UAV moves sideways or forward/backward. This will be felt like movements in the image. A physical stabilized gimbal can be considered for this, as an improvement. With good image quality from the camera sensor, a digital stabilizer can be considered as well. 