# Installing Software
## Operating System
Install operating system by following [these instructions](os.md)

## Configure Zerotier
[Instructions](zerotier.md)

## Configure Flux
[Instructions](flux.md)