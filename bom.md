# Bill of Materials (BOM)
| Component       | Type                    | Vendor        | Amount  | Currency  | Price   | Sum     | Link  |
| ---             | ---                     | ---           | ---     | ---       | ---     | ---     | ---   |
| Frameset        | X500 V2 ARF Kit         | Holybro       | 1       | USD       | 249     | 249     | https://shop.holybro.com/x500-v2-kit_p1288.html |
| Extra Propeller | Propeller 1045          |	Holybro       | 1       | USD       | 11      | 11      | https://shop.holybro.com/spare-parts-x500-v2-kit_p1291.html |
| Pixhawk 6X      | Standard Set + M8N GPS  | Holybro       | 1       | USD       | 379.99  |	379.99  |	https://shop.holybro.com/pixhawk-6x_p1333.html |
| PM03D           | Power Module            | Holybro       | 1       | USD       | 49.09	  | 49.09   |	https://shop.holybro.com/pm03d-power-module_p1315.html |
| Camera          | Raspberry Pi HQ         | Atea          | 1       | NOK       | 763     | 763     |	https://www.atea.no/eshop/product/raspberry-pi-high-quality/?prodid=4797243 |
| Linse           | For HQ camera           |	Komplett      | 1       | NOK       | 649     | 649     |	https://www.komplett.no/product/1160869/datautstyr/pc-komponenter/hovedkort/tilbehoer/raspberry-pi-6mm-wide-angle-lens |
| Camera cable    |	30cm                    | DigitalImpuls | 1       | NOK       | 69      | 69      |	https://www.digitalimpuls.no/kamera/139215/flex-cable-for-raspberry-pi-camera-300mm--12 |
| Raspberry Pi 4  | min. 4GB RAM            | Atea          | 1       | NOK       | 1358    | 1358    |	https://www.atea.no/eshop/product/raspberry-pi-4-model-b/?prodid=4469122 |
| SD card         | 128GB                   | Atea          | 1       | NOK       | 264     | 264     |	https://www.atea.no/eshop/product/sandisk-ultra-flashminnekort/?prodid=4812309 |
| 4G/5G USB modem |	Support for linux       | Proshop       | 1       | NOK       | 590     | 590     |	https://www.proshop.no/Modem-Mobilt-WiFi/Huawei-E3372H-320-4G-USB-modem/2855576 |
| Hand controller |	For Android             | Proshop       | 1       | NOK       | 699     | 699     |	https://www.proshop.no/Spill-tilbehoer/Razer-Kishi-Universal-Gaming-Controller-Android-Black-Gamepad-Android/2860709 |
| Batteri         | 4S 5000mAh              | Elefun        | 1       | NOK       | 699     | 699     |	https://www.elefun.no/p/prod.aspx?v=53639 |
| Lader           | For 4S                  | Elefun        | 1       | NOK       | 895     | 895     |	https://www.elefun.no/p/prod.aspx?v=46578 |
| Security bag    | for batteri             | Elefun        | 1       | NOK       | 149     | 149     |	https://www.elefun.no/p/prod.aspx?v=29944 |
| Cables          | For battery             |	Elefun        | 2       | NOK       | 69      | 138     |	https://www.elefun.no/p/prod.aspx?v=51154 |